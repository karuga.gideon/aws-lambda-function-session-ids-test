package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	getConnectedClients()
}

func getConnectedClients() {
	sessionIDs := []int{6399, 6615, 6515, 6517, 6627, 6295, 6296, 5963, 6304, 6526, 6368, 6377, 6277, 6369, 6378, 6253, 6254, 6257, 6538, 6260, 6382, 6635, 6383, 6389, 6546, 6510, 6393, 6550, 6432, 6499,
		6554, 6439, 6575, 6557, 6558, 6445, 6446, 6447, 6564, 6448, 6452, 6430, 6436, 6475, 6467, 6476, 6468, 6463, 6648, 6610, 5225, 5461, 6308, 6246, 6592, 6604, 6504, 6503, 6505, 6402,
		6555, 6411, 6434, 6583, 6485, 6609, 6330, 6631, 6345, 6379, 6556, 6628, 6587, 6593, 6563, 6320, 6324, 6321, 6384, 6580, 6581, 6313, 6390, 6424, 6425, 6490, 6599, 6603, 6632, 6328,
		6323, 6457, 6516, 6518, 6616, 6613, 6508, 6548, 6551, 6552, 6553, 6584, 6566, 6588, 6649, 6657, 6611, 6326, 6523, 6524, 6527, 6325, 6471, 6469, 6646, 6513, 6608, 6414, 6417, 6579,
		6597, 6598, 6461}

	sessionIDsLen := len(sessionIDs)
	maxIndex := sessionIDsLen - 1
	primaryIterations := int(math.Ceil(float64(sessionIDsLen)) / 100)

	queryString := ""

	for i := 0; i <= primaryIterations; i++ {
		var offSet int
		if i == 0 {
			offSet = 0
		} else {
			offSet = i * 100
		}

		values := make([]string, 0)
		for j := 0; j < 100; j++ {
			currentIndex := j + offSet
			values = append(values, fmt.Sprintf(":%d", currentIndex+1))
			if currentIndex == maxIndex {
				break
			}
		}

		if i == 0 {
			queryString += "session_id IN (" + strings.Join(values, ",") + ")"
		} else {
			queryString += " OR session_id IN (" + strings.Join(values, ",") + ")"
		}
	}

	fmt.Printf("queryString=[%v]\n", queryString)
}
